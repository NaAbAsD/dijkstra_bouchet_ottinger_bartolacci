import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * <b>Controleur est la classe représentant le contrôleur de la l'interface de la comise voyageuse.</b>
 * <p>
 * Ce controleur est caractérisé par les informations suivantes :
 * <ul>
 * <li>Trois CheckBox permettant de sélectionner le type de tas.</li>
 * <li>Un bouton permettant de sélectionner la map où l'on veut lancer Dijkstra.</li>
 * <li>Un bouton permettant de lancer l'algorithme de Dijkstra.</li>
 * <li>Une liste des String des différent noms de villes.</li>
 * <li>Un textfield permettant d'afficher le chemin de la map sélectionnée</li>
 * <li>Un pane permettant d'afficher l'image et de dessiner par dessus</li>
 * <li>Une liste de Ville afin de mieux placer les villes sur la pane</li>
 * </ul>
 * </p>
 * @param <listeVilles>
 * @param <Ville>
 * 
 * @see Ville
 */
public class Controller {
	
	/**
   	 * Les CheckBox permettant de sélectionner le type du tas à utiliser pour l'algorithme de Dijkstra.
	*/
	@FXML private CheckBox _CB1;
	@FXML private CheckBox _CB2;
	@FXML private CheckBox _CB3;
	
	/**
	 * Le bouton permettant de sélectionner la map où l'on veut lancer Dijkstra.
	 */
	@FXML private Button _browseButton;
	
	/**
	 * Un bouton permettant de lancer l'algorithme de Dijkstra.
	 */
	@FXML private Button _dijkstraButton;
	
	/**
	 * Une liste des String des diffï¿œrents noms de villes.
	 */
	@FXML private ListView<String> _list;
	
	/**
	 * Un textfield permettant d'afficher le chemin de la map sélectionnée.
	 */
	@FXML private TextField _textField;
	
	/**
	 * Un pane permettant d'afficher l'image et de dessiner par dessus.
	 */
	@FXML private Pane _pane;
	
	/**
	 * Une liste de Ville afin de mieux placer les villes sur la pane.
	 */
	private ArrayList<Ville> listeVilles = new ArrayList<Ville>();
	
	/**
	 * Chargement de la librairie code C
	 */
	static {
		System.load(System.getProperty("user.dir") + "/Dijkstra.so");
    }
	
	/**
	 * Fonction native du code C
	 * Le dernier paramètre est devenu String pour faciliter le transfert
	 */
	public native void Dijkstra(String in, String out, String source, String type);

	/**
     	* Constructeur ControleurFenetreAccueil.
     	*/
	public Controller() {}

	/**
	 * Primivite d'ajout de Ville dans la liste
	 */
	public boolean add(Ville e) {
		return listeVilles.add(e);
	}

	/**
	 * Primivite de récupération de la liste de Ville
	 */
	public ArrayList<Ville> getListeVilles() {
		return listeVilles;
	}

	/**
	 * Primivite de changement de listeVilles
	 */
	public void setListeVilles(ArrayList<Ville> listeVilles) {
		this.listeVilles = listeVilles;
	}

	/**
	 * Fonction permettant de mettre à jour l'etat des checkbox en fonction de l'activation d'une des CheckBox
	 * Si la CheckBox TableauxDynamiques est activé alors on désactive les deux autres CheckBox
	 */
	public void boxTableauxDynamiques() {
	    if (_CB1.isSelected()) {
	      _CB2.setDisable(true);
	      _CB3.setDisable(true);
	    } else {
	    	_CB2.setDisable(false);
		_CB3.setDisable(false);
	    }
	}

	/**
	 * Fonction permettant de mettre à jour l'état des checkbox en fonction de l'activation d'une des CheckBox
	 * Si la CheckBox ArbresBinaires est activé alors on désactive les deux autres CheckBox
	 */
	public void boxArbresBinaires() {
	    if (_CB2.isSelected()) {
		_CB1.setDisable(true);
		_CB3.setDisable(true);
	    } else {
	    	_CB1.setDisable(false);
		_CB3.setDisable(false);
	    }
	}
	
	/**
	 * Fonction permettant de mettre à jour l'état des checkbox en fonction de l'activation d'une des CheckBox
	 * Si la CheckBox ListesOrdonnees est activé alors on désactive les deux autres CheckBox
	 */
	public void boxListesOrdonnees() {
	    if (_CB3.isSelected()) {
	      _CB1.setDisable(true);
	      _CB2.setDisable(true);
	    } else {
	    	_CB1.setDisable(false);
		_CB2.setDisable(false);
	    }
	}

	/**
	 * Fonction permettant de désactiver toutes les checkBox
	 */
	public void resetCheckBox() {
		_CB1.setDisable(true);
		_CB2.setDisable(true);
		_CB3.setDisable(true);
		_CB1.setSelected(false);
		_CB2.setSelected(false);
		_CB3.setSelected(false);
	}
	
	/**
	 * Initialize du Controller
	 * <p>
	 * A la construction d'un objet Controller
	 * On met l'image de fond et on désactive certains assets
	 * </p>
	 */
	@FXML
	public void initialize() throws FileNotFoundException {
		Class<?> clazz = Controller.class;
		InputStream input = clazz.getResourceAsStream("cerclebleu.png");
		Image image = new Image(input);
		BackgroundImage backGroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT); 
		Background background = new Background(backGroundImage);
		_CB1.setDisable(true);
		_CB2.setDisable(true);
		_CB3.setDisable(true);
		_pane.setBackground(background);
	}
	
	/**
	  * <p>
	  * openFileChooser est appelée lorsque l'on clique sur le bouton "Browse".
	  * Cette méthode ouvre un FileChooser afin séléctionner le fichier map à utiliser.
	  * </p>
	  * 
	  * @see Controller#_list
	  * @see Controller#_pane
	  * @see Controller#_textField
	  * @see Controller#_browseButton
	  */
	@FXML
	public void openFileChooser(){
		listeVilles.clear();
		_list.getItems().clear();
		_pane.getChildren().clear();
		_textField.clear();
		resetCheckBox();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choisissez votre carte");
		fileChooser.setInitialDirectory(new File("../data/"));
		Stage stage = (Stage) _browseButton.getScene().getWindow();
		File file = fileChooser.showOpenDialog(stage);
		if (file != null) {
		    printLog(_textField, file);
		    printLine(_list, file);
		}
	}
	
	/**
	  * <p>
	  * Fonction permettant d'afficher un chemin dans un textField
	  * </p>
	  * 
	  * @param textField
	  * 		   Le caractère à verifier
	  * @param file
	  * 		   Le chemin à afficher
	  */ 
	public void printLog(TextField textField, File file) {
       	if (file == null) return;
       	textField.appendText(file.getAbsolutePath() + "\n");
  	}
	
	/**
	  * <p>
	  * Fonction permettant d'afficher le nom des villes dans un listView
	  * La fonction lit le fichier en paramètre
	  * 	Si le fichier a une ligne suivant alors on affiche la ville lu dans le listView autant de fois qu'il y a de ville
	  * 	On créé des instances de villes pour chaque villes en ajoutant les coordonnés où l'on va dessiner la ville
	  * </p>
	  * 
	  * @param listView
	  * 		   La listView où l'on affiche les villes
	  * @param file
	  * 		   Le chemin à afficher
	  * 
	  * @see Controller#_pane
	  * @see Controller#listeVilles
	  */ 
	public void printLine(ListView<String> listView, File file) {
		if (file == null) return;
		Scanner scanner = null;
		try {
		    scanner = new Scanner(file);
		    if (scanner.hasNextLine()) {
		        String premiereLigne = scanner.nextLine();
		        int nbVilles = Integer.parseInt(premiereLigne.trim());
		        for (int i = 0; i < nbVilles; i++) {
		        	String nomVille = scanner.nextLine();
		        	listView.getItems().add(nomVille);
		        	double[] coordonnees = cordinate_print(nbVilles, i);
		        	Ville ville = new Ville(nomVille, coordonnees[0], coordonnees[1]);
		        	listeVilles.add(ville);
		        	printCity(_pane, ville);
		        }
		    }
		} catch(IOException e) {e.printStackTrace();}
		finally {
		    if(scanner != null) scanner.close();
		}
	}
	
	/**
	  * <p>
	  * Fonction retournant un tableau de double permettant de calculer la position d'une ville dans un cercle
	  * 
	  * @return un tableau de double x,y une position d'une ville calculer en fonction 
	  * 	du nombre de ville max et du numero de la ville actuelle
	  * 
	  * @param nb_of_chunks
	  * 		   L'entier représentant le nombre totale de ville à afficher
	  * @param numVille
	  * 		   L'entier représentant le numero de la ville
	  */ 
	public double[] cordinate_print(int nb_of_chunks, int numVille) {
		double x, y;
		double angle = 0;
		double rad = 225;

		angle = numVille * (360 / nb_of_chunks);
		double radian = (double)(angle * Math.PI / 180);
        x = 300 + rad * Math.cos(radian);
        y = 300 + rad * Math.sin(radian);
        return new double[] {x,y};
	}
	
	/**
	  * <p>
	  * Fonction permettant d'afficher/dessiner une ville sur une pane
	  * Création d'un cercle au position de la ville et ajout d'un label représentant le nom de la ville
	  * </p>
	  * 
	  * @param pane
	  * 		   La pane ou l'on va dessiner la ville.
	  * @param ville
	  * 		   L'instance de Ville à dessiner
	  * 
	  * @see Ville
	  */ 
	public void printCity(Pane pane, Ville ville) {
		Circle circle = makeCircle(ville.getxPos(), ville.getyPos());
        Label label = new Label(); 
        label.setText(ville.getNom());
        label.setLayoutX(ville.getxPos() + 10);
        label.setLayoutY(ville.getyPos());
		pane.getChildren().addAll(circle, label);
	}
	
	/**
	  * <p>
	  * Fonction retournant un cercle permettant de dessiner la position d'une ville dans la pane
	  * </p>
	  * 
	  * @return un cercle pour chaque ville.
	  * 
	  * @param drawX
	  * 		   Le double représentant la position x du cercle à créer
	  * @param drawY
	  * 		   Le double représentant la position y du cercle à créer
	  */ 
	public Circle makeCircle(double drawX, double drawY) {
        Circle circle = new Circle(drawX, drawY, 7, Color.CORAL);
        circle.setStroke(Color.BLACK);
        return circle;
    }
	
	/**
	  * <p>
	  * selectCity est appelée lorsque l'on clique sur une des villes du ListView.
	  * Cette méthode rend les CheckBox non sélectionnés.
	  * </p>
	  * 
	  * @see Controller#_CB1
	  * @see Controller#_CB2
	  * @see Controller#_CB3
	  */
	@FXML
	public void selectCity() {
		_CB1.setSelected(false);
		_CB2.setSelected(false);
		_CB3.setSelected(false);
		_CB1.setDisable(false);
		_CB2.setDisable(false);
		_CB3.setDisable(false);
	}
	
	/**
	  * <p>
	  * runDijkstra est appelée lorsque l'on clique sur "Lancer Dijkstra".
	  * Cette méthode affecte le type de tas, la ville de depart et le chemin de la map aux variables créées pour les stocker puis
	  * lance l'algorithme de Dijkstra implenté par le jni avec ces variables en parametres.
	  * </p>
	  * 
	  * @see Controller#Dijkstra(String, String, String, String)
	  * 
	  * @see Controller#_textField
	  * @see Controller#_list
	  * @see Controller#_CB1
	  * @see Controller#_CB2
	  * @see Controller#_CB3
	  */
	@FXML
	public void runDijkstra(ActionEvent event) {
		String tas = "0";
		String villeDep = "";
		String fileIn = "";
		
		if (_textField.getText() != null && !_textField.getText().trim().equals("")) { // Chemin valide
			fileIn = _textField.getText();
		} else {
			erreur("Fichier non selectionné :");
			return;
		}
		
		if (_list.getSelectionModel().getSelectedItems().size() != 0) { // Ville bien choisie
			villeDep = _list.getSelectionModel().getSelectedItem();
		} else {
			erreur("Ville non indiquée...");
			return;
		}
		
		// Tas bien choisi
		if (_CB1.isSelected()) {
			tas = "0";
		} else if (_CB2.isSelected()) {
			tas = "1";
		} else if (_CB3.isSelected()) {
			tas = "2";
		} else {
			erreur("Tas non sélectionné...");
			return;
		}
		// Execution JNI
		new Controller().Dijkstra(fileIn, "../data/out.txt", villeDep, tas);
		try {
			File fileOut = new File("../data/out.txt");
			printDessiner(fileOut);
		} catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	  * <p>
	  * Fonction permettant de dessiner les routes entre les villes
	  * Cette fonction supprime les éléments du pane pour recréer tous les points de la ville,
	  * Ensuite elle lit le fichier ou sont écrits les routes afin de dessiner une ligne 
	  * reliant les deux villes ecrites sur la ligne du fichier.
	  * </p>
	  * 
	  * @param file
	  * 		   Le fichier à lire afin de récuperer les routes.
	  * 
	  * @see Ville
	  * 
	  * @see Controller#_pane
	  * @see Controller#listeVilles
	  * @see Controller#_list
	  */ 
	public void printDessiner(File file) {
		if (file == null) return;
		_pane.getChildren().clear();
		for (Ville v : listeVilles) {
			printCity(_pane, v);
		}
		Ville ville = rechercheVille(_list.getSelectionModel().getSelectedItem());
		Circle circle = makeMainCircle(ville.getxPos(), ville.getyPos());
		_pane.getChildren().addAll(circle);
		Scanner scanner = null;
		try {
		    scanner = new Scanner(file);
		    if (scanner.hasNextLine()) {
		    	final String SEPARATEUR = " ";
		        String premiereLigne = scanner.nextLine();
		        int nbVilles = Integer.parseInt(premiereLigne.trim());
		        for (int i = 0; i < nbVilles; i++) {
		        	String nomVille = scanner.nextLine();
		        	String mots[] = nomVille.split(SEPARATEUR);
		        	if (mots.length > 1) {
		        		dessiner(rechercheVille(mots[0]), rechercheVille(mots[2]));
		        	}
		        }
		    }
		} catch (IOException e) {e.printStackTrace();}
		finally {
		    if (scanner != null) scanner.close();
		}
	}
	
	/**
	  * <p>
	  * Fonction retournant un cercle permettant de dessiner la position de la ville de départ
	  * </p>
	  * 
	  * @return un cercle pour chaque ville.
	  * 
	  * @param drawX
	  * 		   Le double représentant la position x du cercle à créer
	  * @param drawY
	  * 		   Le double représentant la position y du cercle à créer
	  */ 
	public Circle makeMainCircle(double drawX, double drawY) {
		Circle circle = new Circle(drawX, drawY, 14, Color.CORAL);
		circle.setStroke(Color.BLACK);
		return circle;
    }
	
	/**
	  * <p>
	  * Fonction permettant de recherche une Ville en comparant le nom de la Ville avec le String de la variable
	  * </p>
	  * 
	  * @param ville
	  * 		   La String représentant le nom de la ville à trouver.
	  * 
	  * @see Ville
	  * 
	  * @see Controller#listeVilles
	  */ 
	public Ville rechercheVille(String ville) {
		for (Ville v : listeVilles) {
			if (v.getNom().equals(ville)) {
				return v;
			}
		}
		return null; //N'arrive jamais sans intervention humaine
	}
	
	/**
	  * <p>
	  * Fonction permettant de tracer une ligne entre les positions des deux villes.
	  * </p>
	  * 
	  * @param villeDep
	  * 		   L'instance de la ville de départ
	  * 
	  * @param villeArr
	  * 		   L'instance de la ville de départ
	  * 
	  * @see Ville
	  * 
	  * @see Controller#_pane
	  */ 
	public void dessiner(Ville villeDep, Ville villeArr) {
		Line line = new Line(villeDep.getxPos(), villeDep.getyPos(), villeArr.getxPos(), villeArr.getyPos());
		line.setStrokeWidth(3);
	    line.setStroke(Color.BLACK);
	    _pane.getChildren().add(line);
		
	}
	
	/**
	  * <p>
	  * Fonction permettant d'afficher une boite de dialogue de type erreur comportant le message de l'erreur.
	  * </p>
	  * 
	  * @param message
	  * 		   Le String comportant le type d'erreur.
	  */ 
	public void erreur(String message) {
        Alert dialogA = new Alert(AlertType.WARNING);

        dialogA.getButtonTypes().setAll(ButtonType.OK);
        dialogA.setTitle("Avertissement");
        dialogA.setHeaderText(null); // No header
        dialogA.setContentText(message);
        
        Optional<ButtonType> reponse = dialogA.showAndWait();
        if (reponse.get() == ButtonType.OK) {  
            System.out.println("Bouton OK choisi");
        }
    }
	
}
