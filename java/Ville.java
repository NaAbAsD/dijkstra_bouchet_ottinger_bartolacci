
/**
 * <b>Ville est la classe représentant les villes de la map.</b>
 * <p>
 * Cette classe est caractérisé par les informations suivantes :
 * <ul>
 * <li>Un String qui représente le nom de la ville à enregistré</li>
 * <li>Un Double qui représente la position en x de la ville sur la carte</li>
 * <li>Un Double qui représente la position en y de la ville sur la carte</li>
 * </ul>
 * </p>
 */
public class Ville {

	/**
	 * Le String permettant de garder le nom de la ville
	 */
	private String nom;
	
	/**
	 * Le double permettant de garder la position en x de la ville
	 */
	private double xPos;
	
	/**
	 * Le double permettant de garder la position en y de la ville
	 */
	private double yPos;
	
	/**
     * Constructeur Ville.
     * <p>
     * A la construction d'un objet Ville, on set les différents éléments de Ville 
     * </p>
     * 
     * @see Ville#nom
     * @see Ville#xPos
     * @see Ville#yPos
     */
	public Ville(String nom, double xPos, double yPos) {
		setNom(nom);
		setxPos(xPos);
		setyPos(yPos);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getxPos() {
		return xPos;
	}

	public void setxPos(double xPos) {
		this.xPos = xPos;
	}

	public double getyPos() {
		return yPos;
	}

	public void setyPos(double yPos) {
		this.yPos = yPos;
	}

	@Override
	public String toString() {
		return "Ville [nom=" + nom + ", xPos=" + xPos + ", yPos=" + yPos + "]";
	}
	
}
