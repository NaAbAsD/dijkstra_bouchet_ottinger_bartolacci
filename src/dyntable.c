#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"

DTable * newDTable() {
	DTable * newTable = (DTable *)calloc(1, sizeof(DTable));
	newTable->T = (void **)calloc(1, sizeof(void *));
	newTable->size = 1;
	newTable->used = 0;
	return newTable;
}

/**
 * @brief
 * Dédoubler la taille du tableau dtab
 */
static void scaleUp(DTable *dtab) {
	dtab->size = dtab->size * 2;
	dtab->T = realloc(dtab->T, dtab->size * sizeof(void *));
}

/**
 * @brief
 * Diviser par 2 la taille du tableau dtab
 */
static void scaleDown(DTable *dtab) {
	dtab->size = dtab->size / 2;
	dtab->T = realloc(dtab->T, dtab->size * sizeof(void *));
}

void viewDTable(const DTable *dtab, void (*ptrf)()) {
	printf("size = %d\n", dtab->size);
	printf("used = %d\n", dtab->used);
	for (int i = 0; i < dtab->used; i++) {
		ptrf(dtab->T[i]);
		printf(" ");
	}
	printf("\n");
}

void DTableInsert(DTable *dtab, void *data) {
	if (dtab->size == dtab->used) {
		scaleUp(dtab);
	}
	dtab->T[dtab->used] = data;
	dtab->used++;
}

void * DTableRemove(DTable *dtab) {
	assert(dtab->used > 0);
	void * removed = dtab->T[dtab->used - 1];
	dtab->used--;
	if (dtab->used <= dtab->size / 4) {
		scaleDown(dtab);
	}
	return removed;
}

void DTableSwap(DTable *dtab, int pos1, int pos2) {
	assert(pos1 < dtab->used);
	assert(pos2 < dtab->used);
	void * aux = dtab->T[pos1];
	dtab->T[pos1] = dtab->T[pos2];
	dtab->T[pos2] = aux;
}
