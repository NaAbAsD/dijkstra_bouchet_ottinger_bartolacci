#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"
#include "test.h"

/**
 * Exemple d'une fonction qui affiche le contenu d'un HNode.
 * A modifier si besoin.
 */
void viewHNode(const HNode *node) {
	struct town * town = node->data;
	printf("(%d, %s)", node->value, getTownName(town));
}

/**
 * Affichage de la solution de l'algorithme de Dijkstra.
 * Pour chaque ville du graphe G on doit déjà avoir défini
 * les valeurs dist et pred en exécutant l'algorithme de Dijkstra.
 */
void viewSolution(graph G, char * sourceName) {
	printf("Distances from %s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			printf("%s : %d km (via %s)\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
}

/**
 * Modification de la fonction d'au dessus pour permettre l'affichage dans le stdout ou l'écriture dans le fichier
 */
void viewSolutionChoix(FILE * out, graph G, char * sourceName, int heaptype) {
	fprintf(out, "%d\n", G->numelm);
	fprintf(out, "%s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			fprintf(out, "%s %d %s\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
	fprintf(out, "\n");
	if (out != stdout) fclose(out); // Si ce n'est pas la sortie standard, on la ferme
}

int heapElemCount(Heap * H, int heaptype) {
	switch (heaptype) {
		case (0): {
			DTable * heapContainer = H->heap;
			return heapContainer->used;
		}
		case (1): {
			CBTree * heapContainer = H->heap;
			return heapContainer->numelm;
		}
		case (2): {
			List * heapContainer = H->heap;
			return heapContainer->numelm;
		}
		default: return -1; // N'arrivera jamais - type invalide
	}
}

/**
 * Algorithme de Dijkstra
 * inFileName : nom du fichier d'entrée
 * outFileName : nom du fichier de sortie
 * sourceName : nom de la ville de départ
 * heaptype : type du tas {0--tableaux dynamiques, 1--arbres binaires complets, 2--listes ordonnées}
 */
void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype) {
	graph G = readmap(inFileName);
	FILE * res = fopen(outFileName, "w");
	// Initialisation
	struct town * actualTown;
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		actualTown = iterator->data;
		if (strcmp(actualTown->name, sourceName) != 0) { // Ce ne sont pas les mêmes
			actualTown->dist = INFTY;
		} else {
			actualTown->dist = 0;
		} 
		actualTown->pred = NULL;
	}

	// Temps d'execution
	clock_t begin = clock();

	// Création de la file
	Heap * H = newHeap(heaptype);
	for (iterator = G->head; iterator; iterator = iterator->suc) { // Copie des objets dans le tas
		actualTown = iterator->data;
		actualTown->ptr = H->HeapInsert(H, actualTown->dist, actualTown);
	}
	// Boucle principale
	HNode * extracted;
	struct road * actualRoad;
	struct town * otherTown;
	for (int i = heapElemCount(H, heaptype); i > 0; i--) { // Application de Dijkstra
		extracted = H->HeapExtractMin(H); // Attention à la libération
		actualTown = extracted->data;
		for (iterator = actualTown->alist->head; iterator; iterator = iterator->suc) { // Recherche du plus court
			actualRoad = iterator->data;
			otherTown = getURoad(actualRoad) == actualTown ? getVRoad(actualRoad) : getURoad(actualRoad);
			if (otherTown->dist > actualRoad->km + actualTown->dist) { 
				otherTown->dist = actualRoad->km + actualTown->dist;
				otherTown->pred = actualTown;
		  		H->HeapIncreasePriority(H, otherTown->ptr, otherTown->dist);
			}
		}
		free(extracted);
	}

	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  	printf("Dijkstra time = %lf\n", time_spent);
	
	// Affichage du résultat
	viewSolutionChoix(res, G, sourceName, heaptype);
	// Libération de la mémoire
	// Dict
	free(H->dict->T);
	free(H->dict);
	// Heap
	if (heaptype == 0) {
		free(((DTable *)H->heap)->T);
	}
	free(H->heap);
	free(H);
	// Graph
	freeGraph(G);
}

int main() {
	int nbTowns = 10;
	char * towns[10] = {"Paris", "Lyon", "Metz", "Nancy", "Reims", "Grenoble", "Valence", "Chambery", "Dijon", "Besançon"};
	printf("Les solutions pour chaque ville (dans l'ordre du fichier map) sont stockées dans le fichier data/out.txt dans l'ordre :\n1 - DTable\n2 - CBTree\n3 - List\n");
	for (int i = 0; i < nbTowns; i++) {
		for (int j = 0; j <= 2; j++) {
			Dijkstra("data/map2.txt", "data/out.txt", towns[i], j);
		}
	}
	// Dijkstra("data/map_500_50_10000_5", "data/out.txt", "001", 0);
	// Dijkstra("data/map_500_50_10000_5", "data/out.txt", "001", 1);
	// Dijkstra("data/map_500_50_10000_5", "data/out.txt", "001", 2);
	return EXIT_SUCCESS;
}
