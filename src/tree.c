#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tree.h"
#include "town.h"

TNode * newTNode(void* data) {
    TNode * node = (TNode *)calloc(1, sizeof(TNode));
    node->data = data;
    return node;
}
 
CBTree * newCBTree() {
    CBTree * CBT = (CBTree *)calloc(1, sizeof(CBTree));
    return CBT;
}

static void preorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		ptrF(node->data);
		printf(" ");
		preorder(node->left, ptrF);
		preorder(node->right, ptrF);
	}
}

static void inorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		inorder(node->left, ptrF);
		ptrF(node->data);
		printf(" ");
		inorder(node->right, ptrF);
	}
}

static void postorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		postorder(node->left, ptrF);
		postorder(node->right, ptrF);
		ptrF(node->data);
		printf(" ");
	}
}

// order = 0 (preorder), 1 (postorder), 2 (inorder)
void viewCBTree(const CBTree* tree, void (*ptrF)(), int order) {
	assert(order == 0 || order == 1 || order == 2);
	printf("nb of nodes = %d\n", tree->numelm);
	switch (order) {
		case 0:
			preorder(tree->root, ptrF);
			break;
		case 1:
			postorder(tree->root, ptrF);
			break;
		case 2:
			inorder(tree->root, ptrF);
		break;
	}
	printf("\n");
}

void CBTreeInsert(CBTree * tree, void * data) {
    TNode * node = newTNode(data);
    TNode * lastNode = tree->last;
    if (lastNode == NULL) {
        // Insertion dans un arbre vide
        tree->root = node;
    } else if (tree->numelm == 1) {
        // On insère à gauche de la racine
        tree->root->left = node;
        node->parent = tree->root;
    } else if (lastNode == lastNode->parent->left) {
        // Il y a une place à la droite du dernier élément
        lastNode->parent->right = node;
        node->parent = lastNode->parent;
    } else {
        // On remonte jusqu'à trouver un noeud qui est fils gauche de son parent
        // Ou au pire des cas, jusqu'à la racine
        while (lastNode != tree->root && lastNode == lastNode->parent->right) {
            lastNode = lastNode->parent;
        }
        if (lastNode != tree->root) {
            lastNode = lastNode->parent->right;
        }
        while (lastNode->left != NULL) {
            lastNode = lastNode->left; // On descend tout à gauche
        }
        lastNode->left = node;
        node->parent = lastNode;
    }
    tree->last = node;
    tree->numelm++;
}

void * CBTreeRemove(CBTree* tree) {
    assert(tree->last != NULL);
    TNode * lastNode = tree->last, * toDelete = tree->last;
    void * data = lastNode->data;
    if (tree->numelm == 1) {
        // On extrait l'unique élément de l'arbre
        tree->last = NULL;
        tree->root = NULL;
    } else {
        if (lastNode == lastNode->parent->right) {
            // L'avant dernier élément est le frère de lastNode
            lastNode->parent->right = NULL;
            tree->last = lastNode->parent->left;
        } else {
            // Le dernier élément est le fils gauche de root
            if (lastNode->parent == tree->root) {
                tree->last = tree->root;
                tree->root->left = NULL;
            } else {
                // On remonte jusqu'à trouver un noeud qui est fils droit de son parent
                // Ou au pire des cas, jusqu'à la racine
                lastNode = lastNode->parent;
                lastNode->left = NULL;
                while (lastNode != tree->root && lastNode == lastNode->parent->left) {
                    lastNode = lastNode->parent;
                }
                if (lastNode != tree->root) {
                    lastNode = lastNode->parent->left;
                }
                while (lastNode->right != NULL) {
                    lastNode = lastNode->right; // On descend tout à droite
                }
                tree->last = lastNode;
            }
        }
    }
    tree->numelm--;
    free(toDelete);
    return data;
}

void CBTreeSwap(CBTree* tree, TNode* parent, TNode* child) {
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));
    if (child == tree->last) {
        tree->last = parent;
    }
    if (parent == tree->root) {
        tree->root = child;
    } else { //On change le père de parent
        if (parent == parent->parent->left) {
            parent->parent->left = child;
        } else {
            parent->parent->right = child;
        }
    }
    // On change le père du frère éventuel de child
    TNode *stockL, *stockR;
    if (child == parent->left) {
        stockR = parent->right;
        stockL = parent;
        if (parent->right != NULL) {
            parent->right->parent = child;
        }
    } else {
        stockR = parent;
        stockL = parent->left;
        if (parent->left != NULL) {
            parent->left->parent = child;
        }
    }
    // On change le père des fils éventuels de child
    if (child->left != NULL) {
        child->left->parent = parent;
    }
    if (child->right != NULL) {
        child->right->parent = parent;
    }
 
    // Échange de tous les pointeurs qui partent de child et parent
    TNode* stockP = parent->parent;
    parent->parent = child;
    parent->right = child->right;
    parent->left = child->left;
    child->parent = stockP;
    // Problème ici, comme dans SwapRootLast - B boucle sur lui même car gauche pointe sur lui même
    child->left = stockL;
    child->right = stockR;
}

void CBTreeSwapRootLast(CBTree* tree) {
    if (tree->numelm > 1) {
        if (tree->root->left == tree->last || tree->root->right == tree->last) {
        CBTreeSwap(tree, tree->root, tree->last);
        } else {
            tree->last->left = tree->root->left; // Gauche nouvelle root
            tree->last->right = tree->root->right; // Droite nouvelle root
            tree->root->left->parent = tree->last; // Parent du fils gauche de root
            tree->root->right->parent = tree->last; // Parent du fils droit de root
            if (tree->last->parent->left == tree->last) { // Fils gauche ou droit du parent de last
                tree->last->parent->left = tree->root;
            } else {
                tree->last->parent->right = tree->root;
            }
            tree->root->parent = tree->last->parent; // Parent ancienne root
            tree->root->left = NULL; // Gauche ancienne root
            tree->root->right = NULL; // Droite ancienne root
            tree->last->parent = NULL; // Parent nouvelle root
            // Echange root-last
            TNode * aux = tree->root;
            tree->root = tree->last;
            tree->last = aux;
        }
    }
}