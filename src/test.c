#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include "list.h"
#include "town.h"
#include "tree.h"
#include "dyntable.h"
#include "heap.h"

static int compare_lists(List *l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode *curr = l1->head;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->suc;
		i++;
	}

	curr = l1->tail;
	i = size-1;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->pred;
		i--;
	}
	return 1;
}

void test_list() {
	int t = 0;
	int f = 0;

	fprintf(stderr,"Testing list functions ");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int tab[4] = {*i1,*i2,*i3,*i4};
	int t1[1] = {*i1};
	int t2[2] = {*i2,*i1};
	int t3[3] = {*i3,*i2,*i1};
	int t4[4] = {*i4,*i3,*i2,*i1};

	List *L = newList();
	listInsertLast(L, (int*) i1);
	if (compare_lists(L, tab, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i2);
	if (compare_lists(L, tab, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i3);
	if (compare_lists(L, tab, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i4);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	deleteList(L, NULL);
	if (compare_lists(L, tab, 0) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	listInsertFirst(L, (int*) i1);
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i2);
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i3);
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i4);
	if (compare_lists(L, t4, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
}


/* ****************************************
 * PARTIE 1 : LES LISTES DOUBLEMENT CHAINEES
 * ****************************************
 */

void viewListReverse(const List * L, void (*ptrF)()) {
	printf("nb of nodes = %d\n", L->numelm);
	for(LNode * iterator = L->tail; iterator; iterator = iterator->pred) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
	printf("\n");
}

void testListSuppression() {
	List * L = newList();
	listInsertFirst(L, createTown("H"));
	listInsertFirst(L, createTown("G"));
	LNode * G = L->head; // Le G
	listInsertFirst(L, createTown("F"));
	listInsertFirst(L, createTown("E"));
	listInsertFirst(L, createTown("D"));
	LNode * D = L->head; // Le D
	listInsertFirst(L, createTown("C"));
	listInsertFirst(L, createTown("B"));
	LNode * B = L->head; // Le B
	listInsertFirst(L, createTown("A"));

	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On supprime Le H ***\n");
	listRemoveLast(L);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On supprime Le A ***\n");
	listRemoveFirst(L);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On supprime Le D ***\n");
	listRemoveNode(L, D);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On supprime Le G ***\n");
	listRemoveNode(L, G);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On supprime Le B ***\n");
	listRemoveNode(L, B);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
}

void testListSwap() {
	List * L = newList();
	listInsertFirst(L, createTown("H"));
	LNode * H = L->head; // Le H
	listInsertFirst(L, createTown("G"));
	LNode * G = L->head; // Le G
	listInsertFirst(L, createTown("F"));
	listInsertFirst(L, createTown("E"));
	LNode * E = L->head; // Le E
	listInsertFirst(L, createTown("D"));
	LNode * D = L->head; // Le D
	listInsertFirst(L, createTown("C"));
	listInsertFirst(L, createTown("B"));
	LNode * B = L->head; // Le B
	listInsertFirst(L, createTown("A"));
	LNode * A = L->head; // Le A

	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On swappe D et E ***\n");
	listSwap(L, D, E);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On swappe A et B ***\n");
	listSwap(L, A, B);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
	printf("*** On swappe G et H ***\n");
	listSwap(L, G, H);
	viewList(L, &printTownName);
	viewListReverse(L, &printTownName);
}


/* ****************************************
 * PARTIE 2 : LES ARBES BINAIRES
 * ****************************************
 */

void testCBTreeInsert() {
	// Demander si on peut ajouter affichage first et last dans viewCBTree
	CBTree * tree = newCBTree();
	// 0 élément
	printf("Affichage de l'arbre vide de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	// 1 élément
	CBTreeInsert(tree, createTown("A"));
	printf("Affichage de l'arbre à un élément de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 2 éléments
	CBTreeInsert(tree, createTown("B"));
	printf("Affichage de l'arbre à deux éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à deux éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à deux éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("Last parent : %s\n", getTownName(tree->last->parent->data));
	// 3 éléments
	CBTreeInsert(tree, createTown("C"));
	printf("Affichage de l'arbre à trois éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à trois éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à trois éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 4 éléments
	CBTreeInsert(tree, createTown("D"));
	printf("Affichage de l'arbre à quatre éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à quatre éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à quatre éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 8 éléments
	CBTreeInsert(tree, createTown("E"));
	CBTreeInsert(tree, createTown("F"));
	CBTreeInsert(tree, createTown("G"));
	CBTreeInsert(tree, createTown("H"));
	printf("Affichage de l'arbre à huit éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à huit éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à huit éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
}

void testCBTreeSuppression() {
	CBTree * tree = newCBTree();
	void * deleted;
	// 1 élément
	CBTreeInsert(tree, createTown("A"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("*** On supprime Le A ***\n");
	deleted = CBTreeRemove(tree);
	printf("On a supprimé : %s\n", getTownName(deleted));
	printf("Affichage de l'arbr de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("\n");
	// 2 éléments
	CBTreeInsert(tree, createTown("A"));
	CBTreeInsert(tree, createTown("B"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("*** On supprime Le B ***\n");
	deleted = CBTreeRemove(tree);
	printf("On a supprimé : %s\n", getTownName(deleted));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("\n");
	// 3 éléments
	CBTreeInsert(tree, createTown("B"));
	CBTreeInsert(tree, createTown("C"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("*** On supprime Le C ***\n");
	deleted = CBTreeRemove(tree);
	printf("On a supprimé : %s\n", getTownName(deleted));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("\n");
	// 4 éléments
	CBTreeInsert(tree, createTown("C"));
	CBTreeInsert(tree, createTown("D"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("*** On supprime Le D ***\n");
	deleted = CBTreeRemove(tree);
	printf("On a supprimé : %s\n", getTownName(deleted));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("\n");
	// 8 éléments
	CBTreeInsert(tree, createTown("D"));
	CBTreeInsert(tree, createTown("E"));
	CBTreeInsert(tree, createTown("F"));
	CBTreeInsert(tree, createTown("G"));
	CBTreeInsert(tree, createTown("H"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("*** On supprime Le H ***\n");
	deleted = CBTreeRemove(tree);
	printf("On a supprimé : %s\n", getTownName(deleted));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
}

void testCBTreeSwap() {
	CBTree * tree = newCBTree();
	// 2 éléments
	CBTreeInsert(tree, createTown("A"));
	CBTreeInsert(tree, createTown("B"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("*** On swappe A et B ***\n");
	CBTreeSwap(tree, tree->root, tree->last);
	printf("\nAffichage de l'arbre  de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 3 éléments
	CBTreeInsert(tree, createTown("C"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("*** On swappe B et C ***\n");
	CBTreeSwap(tree, tree->root, tree->last);
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 4 éléments
	CBTreeInsert(tree, createTown("D"));
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("*** On swappe A et D ***\n");
	CBTreeSwap(tree, tree->root->left, tree->root->left->left);
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
}

void testCBTreeSwapRootLast() {
	CBTree * tree = newCBTree();
	// 2 éléments
	CBTreeInsert(tree, createTown("A"));
	CBTreeInsert(tree, createTown("B"));
	printf("Affichage de l'arbre à deux éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à deux éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à deux éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	CBTreeSwapRootLast(tree);
	printf("*** On swappe A et B ***\n");
	printf("Affichage de l'arbre à deux éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à deux éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à deux éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 3 éléments
	CBTreeInsert(tree, createTown("C"));
	printf("Affichage de l'arbre à trois éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à trois éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à trois éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	CBTreeSwapRootLast(tree);
	printf("*** On swappe B et C ***\n");
	printf("Affichage de l'arbre à trois éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à trois éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à trois éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 4 éléments
	CBTreeInsert(tree, createTown("D"));
	printf("Affichage de l'arbre à quatre éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à quatre éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à quatre éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	printf("*** On swappe A et D ***\n");
	CBTreeSwapRootLast(tree);
	printf("Affichage de l'arbre à deux éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à deux éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à deux éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	// 8 éléments
	CBTreeInsert(tree, createTown("E"));
	CBTreeInsert(tree, createTown("F"));
	CBTreeInsert(tree, createTown("G"));
	CBTreeInsert(tree, createTown("H"));
	printf("Affichage de l'arbre à huit éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à huit éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à huit éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
	CBTreeSwapRootLast(tree);
	printf("*** On swappe D et H ***\n");
	printf("Affichage de l'arbre à huit éléments de façon préfixée\n");
	viewCBTree(tree, &printTownName, 0);
	printf("Affichage de l'arbre à huit éléments de façon postfixée\n");
	viewCBTree(tree, &printTownName, 1);
	printf("Affichage de l'arbre à huit éléments de façon infixée\n");
	viewCBTree(tree, &printTownName, 2);
	printf("Root : %s - Last : %s\n", getTownName(tree->root->data), getTownName(tree->last->data));
}


/* ****************************************
 * PARTIE 3 : LES TABLEAUX DYNAMIQUES
 * ****************************************
 */

void testDynTableInsertRemoveSwap(){
	DTable * tab = newDTable();
	printf("\nAffichage du tableau Vide : \n");
	viewDTable(tab, &printTownName);
	DTableInsert(tab, createTown("A"));
	printf("\nAffichage du tableau à 1 élément : \n");
	viewDTable(tab, &printTownName);
	DTableInsert(tab, createTown("B"));
	printf("\nAffichage du tableau à 2 éléments : \n");
	viewDTable(tab, &printTownName);
	DTableInsert(tab, createTown("C"));
	DTableInsert(tab, createTown("D"));
	printf("\nAffichage du tableau à 4 éléments : \n");
	viewDTable(tab, &printTownName);
	
	printf("\n*** Remove des elements ***\n");
	DTableRemove(tab);
	printf("\nAffichage du tableau apres remove d'un élément :\n");
	viewDTable(tab, &printTownName);
	DTableRemove(tab);
	printf("\nAffichage du tableau apres remove de deux éléments afin de scaleDown :\n");
	viewDTable(tab, &printTownName);
	
	printf("\n*** Echange des elements ***\n");
	printf("\nAffichage du tableau apres échange de deux éléments : \n");
	DTableSwap(tab, 0, 1);
	viewDTable(tab, &printTownName);
}


/* ****************************************
 * PARTIE 4 : LES TAS SOUS FORME DE LISTE
 * ****************************************
 */

void viewHNodeTest(const HNode *node) {
	// Pour ne pas importer le main.c car un peu moyen
	struct town * town = node->data;
	printf("(%d - %s)", node->value, getTownName(town));
}

void testOLHeapInsertChangePrioExtract() {
	Heap * H = newHeap(2);
	// 0 élément
	printf("Affichage de la liste vide\n");
	H->viewHeap(H, &viewHNodeTest);
	// 1 élément
	H->HeapInsert(H, 4, createTown("A"));
	printf("Affichage de la liste à un élément\n");
	H->viewHeap(H, &viewHNodeTest);
	// 2 éléments
	H->HeapInsert(H, 2, createTown("B"));
	printf("Affichage de la liste à deux éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 3 éléments
	H->HeapInsert(H, 6, createTown("C"));
	printf("Affichage de la liste à trois éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 4 éléments
	H->HeapInsert(H, 4, createTown("D"));
	printf("Affichage de la liste à quatre éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// Changement de priorité
	List * OL = H->heap;
	printf("*** On change la priorité de A ***\n");
	H->HeapIncreasePriority(H, OL->head->suc, 8);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de C ***\n");
	H->HeapIncreasePriority(H, OL->head->suc->suc, 1);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de B ***\n");
	H->HeapIncreasePriority(H, OL->head->suc, 6);
	H->viewHeap(H, &viewHNodeTest);
	// Suppression d'éléments
	printf("*** On supprime Le C ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On supprime Le D ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On supprime Le B ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On supprime Le A ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
}


/* ****************************************
 * PARTIE 5 : LES TAS SOUS FORME D'ARBRES
 * ****************************************
 */

void testCBTHeapInsertChangePrioExtract() {
	Heap * H = newHeap(1);
	CBTree * tree = H->heap;
	// 0 élément
	printf("Affichage de l'arbre vide\n");
	H->viewHeap(H, &viewHNodeTest);
	// 1 élément
	H->HeapInsert(H, 12, createTown("A"));
	printf("Affichage de l'arbre à un élément\n");
	H->viewHeap(H, &viewHNodeTest);
	// 2 éléments
	H->HeapInsert(H, 2, createTown("B"));
	printf("Affichage de l'arbre à deux éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 3 éléments
	H->HeapInsert(H, 8, createTown("C"));
	printf("Affichage de l'arbre à trois éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 4 éléments
	H->HeapInsert(H, 4, createTown("D"));
	printf("Affichage de l'arbre à quatre éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 8 éléments
	H->HeapInsert(H, 4, createTown("E"));
	H->HeapInsert(H, 1, createTown("F"));
	H->HeapInsert(H, 20, createTown("G"));
	H->HeapInsert(H, 8, createTown("H"));
	printf("Affichage de l'arbre à huit éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// Changement de priorité
	printf("*** On change la priorité de F ***\n");
	H->HeapIncreasePriority(H, tree->root, 8);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de A ***\n");
	H->HeapIncreasePriority(H, tree->last, 1);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de D ***\n");
	H->HeapIncreasePriority(H, tree->last->parent, 6);
	H->viewHeap(H, &viewHNodeTest);
	// Suppression d'éléments
	printf("*** On supprime Le A ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On supprime Le B ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
}


/* ****************************************
 * PARTIE 6 : LES TAS SOUS FORME DE TABLEAUX
 * ****************************************
 */

void testDTHeapInsertChangePrioExtract() {
	Heap * H = newHeap(0);
	DTable * table = H->heap;
	// 0 élément
	printf("Affichage du tableau vide\n");
	H->viewHeap(H, &viewHNodeTest);
	// 1 élément
	H->HeapInsert(H, 12, createTown("A"));
	printf("Affichage du tableau à un élément\n");
	H->viewHeap(H, &viewHNodeTest);
	// 2 éléments
	H->HeapInsert(H, 2, createTown("B"));
	printf("Affichage du tableau à deux éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 3 éléments
	H->HeapInsert(H, 8, createTown("C"));
	printf("Affichage du tableau à trois éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// 4 éléments
	H->HeapInsert(H, 4, createTown("D"));
	printf("Affichage du tableau à quatre éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	H->HeapInsert(H, 4, createTown("E"));
	H->HeapInsert(H, 1, createTown("F"));
	H->HeapInsert(H, 20, createTown("G"));
	H->HeapInsert(H, 8, createTown("H"));
	printf("Affichage du tableau à huit éléments\n");
	H->viewHeap(H, &viewHNodeTest);
	// Changement de priorité
	printf("*** On change la priorité de F ***\n");
	H->HeapIncreasePriority(H, ((HNode *)table->T[0])->ptr, 8);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de A ***\n");
	H->HeapIncreasePriority(H, ((HNode *)table->T[table->used - 1])->ptr, 1);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On change la priorité de D ***\n");
	H->HeapIncreasePriority(H, ((HNode *)table->T[indPere(table->used - 1)])->ptr, 6);
	H->viewHeap(H, &viewHNodeTest);
	// Suppression d'éléments
	printf("*** On supprime Le A ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
	printf("*** On supprime Le B ***\n");
	H->HeapExtractMin(H);
	H->viewHeap(H, &viewHNodeTest);
}
