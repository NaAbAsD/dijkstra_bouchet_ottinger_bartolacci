#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

List * newList() {
	List * L = (List *)calloc(1, sizeof(List));
	return L;
}

void deleteList(List * L, void (*ptrF)()) {
	LNode * iterator = L->head;

	if (ptrF == NULL) {
		while (iterator) {
			LNode * current = iterator;

			iterator = iterator->suc;
			free(current);
		}
	} else {
		while (iterator) {
			LNode * current = iterator;

			(*ptrF)(&current->data);
			free(current);
		}
	}
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
}

void viewList(const List * L, void (*ptrF)()) {
	printf("nb of nodes = %d\n", L->numelm);
	for(LNode * iterator = L->head; iterator; iterator = iterator->suc) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
	printf("\n");
}

void listInsertFirst(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->suc = L->head;

	if(L->head == NULL)
		L->tail = E;
	else
		L->head->pred = E;
	L->head = E;
	L->numelm += 1;
}

void listInsertLast(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = L->tail;

	if(L->tail == NULL)
		L->head = E;
	else
		L->tail->suc = E;
	L->tail = E;
	L->numelm += 1;
}

void listInsertAfter(List * L, void * data, LNode * ptrelm) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = ptrelm;

	if(ptrelm == L->tail)
		L->tail = E;
	else
		ptrelm->suc->pred = E;
	E->suc = ptrelm->suc;
	ptrelm->suc = E;
	L->numelm += 1;
}

LNode* listRemoveFirst(List * L) {
	assert(L->head);
	LNode * removed = L->head;
	if (L->numelm == 1) {
		L->head = NULL;
		L->tail = NULL;
	} else {
		LNode * suivant = L->head->suc;
		L->head = suivant;
		L->head->pred = NULL;
	}
	L->numelm--;
	return removed;
}

LNode* listRemoveLast(List * L) {
	assert(L->head);
	LNode * removed = L->head;
	if (L->numelm == 1) {
		L->head = NULL;
		L->tail = NULL;
	} else {
		LNode * precedent = L->tail->pred;
		L->tail = precedent;
		L->tail->suc = NULL;
	}
	L->numelm--;
	return removed;
}

LNode* listRemoveNode(List * L, LNode * node) {
	if (node == L->head) { // Premier élément
		return listRemoveFirst(L);
	} else if (node == L->tail) { // Dernier élément
		return listRemoveLast(L);
	} else { // Cas général
		LNode * pred = node->pred, * suc = node->suc;
		pred->suc = suc;
		suc->pred = pred;
		L->numelm--;
		return node;
	}
}

void listSwap(List * L, LNode * left, LNode * right) {
	assert(left->suc == right && left == right->pred);
	assert(L->numelm >= 2); // Sinon on ne peut rien faire
	if (L->head != left) { // Cas général
		left->pred->suc = right;
	} else { // Left était le premier élément
		L->head = right;
	}
	if (L->tail != right) { // Cas général
		right->suc->pred = left;
	} else { // Right était le dernier élément
		L->tail = left;
	}
	right->pred = left->pred;
	left->suc = right->suc;
	left->pred = right;
	right->suc = left;
}