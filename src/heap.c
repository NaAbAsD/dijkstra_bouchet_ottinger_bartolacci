#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"

HNode * newHNode(int value, void *data) {
	HNode * node = (HNode *)calloc(1, sizeof(HNode));
    node->data = data;
	node->value = value;
    return node;
}

// type =
//    0 (DynTableHeap)
//    1 (CompleteBinaryTreeHeap)
//    2 (ListHeap)
Heap * newHeap(int type) {
	assert(type == 0 || type == 1 || type == 2);
	Heap* H = calloc(1, sizeof(Heap));
	H->dict = newDTable();
	switch (type) {
		case 0:
			H->heap = newDTable();
			H->HeapInsert = DTHeapInsert;
			H->HeapExtractMin = DTHeapExtractMin;
			H->HeapIncreasePriority = DTHeapIncreasePriority;
			H->viewHeap = viewDTHeap;
			break;
		case 1:
			H->heap = newCBTree();
			H->HeapInsert = CBTHeapInsert;
			H->HeapExtractMin = CBTHeapExtractMin;
			H->HeapIncreasePriority = CBTHeapIncreasePriority;
			H->viewHeap = viewCBTHeap;
			break;
		case 2:
			H->heap = newList();
			H->HeapInsert = OLHeapInsert;
			H->HeapExtractMin = OLHeapExtractMin;
			H->HeapIncreasePriority = OLHeapIncreasePriority;
			H->viewHeap = viewOLHeap;
			break;
	}
	return H;
}

/**********************************************************************************
 * DYNAMIC TABLE HEAP
 **********************************************************************************/

int indPere(int indActuel) {
	if (indActuel == 0) { // On a fini le travail
		return -1;
	}
	return (indActuel - 1) / 2;
}

int indFilsG(int indActuel) {
	return (2 * indActuel) + 1;
}

void DTableSwapBoth(DTable * table, DTable * dictionnary, int i, int j) {
	// Swap classique
	DTableSwap(table, i, j);
	// Swap dans le dictionnaire
	HNode * A,  * B;
	int * ptrA, * ptrB;
    A = table->T[i]; B = table->T[j];
    ptrA = A->ptr; ptrB = B->ptr;
	DTableSwap(dictionnary, *ptrA, *ptrB);
}

void* DTHeapInsert(Heap *H, int value, void *data) {
    HNode * node = newHNode(value, data);
    DTable * table = H->heap;
    DTable * dictionnary = H->dict;
    DTableInsert(table, node);
    // Insertion dans le dictionnaire et construction des "indices de référence"
    int * ind = malloc(sizeof(int)), * indValue = malloc(sizeof(int));
    *ind = table->used - 1, *indValue = table->used - 1;
    DTableInsert(dictionnary, indValue);
    node->ptr = ind;
    int emplacement = *ind; // Emplacement est l'indice de node dans le tableau table
	int pere = indPere(emplacement); // Père est l'indice du père de node dans le tableau table
    HNode *nodePere = table->T[pere];
    while (pere >= 0 && node->value < nodePere->value) { //Tant que la racine n'est pas atteinte et qu'on a pas trouvé l'emplacement de la nouvelle node
        DTableSwapBoth(table, dictionnary, emplacement, pere); // On intervertit la position de la node et de son père (avec mise à jour du dictionnaire)
		// Mise à jour des indices pour la prochaine itération
        emplacement = pere;
        pere = indPere(emplacement);
        nodePere = table->T[pere];
    }
    return ind; // cf. mail
}

HNode * DTHeapExtractMin(Heap* H) {
	DTable * table = H->heap;
    DTable * dictionnary = H->dict;
	// Extraction de la valeur
	DTableSwapBoth(table, dictionnary, 0, table->used - 1);
	HNode * extracted = DTableRemove(table);
	// On prépare le swap
	HNode * toSwap, * nodeFG, * nodeFD, * minFils;
	toSwap = table->T[0];
	int * pos = toSwap->ptr;
    int emplacement = *((int *)dictionnary->T[*pos]); // Emplacement est l'indice de node dans le tableau table
	int fg = indFilsG(emplacement), fd = fg + 1, minFilsInd; // fg, fd sont les indices des fils de node dans le tableau table
	while (fd < table->used) { // Cas général : les deux fils non NULL
		nodeFG = table->T[fg]; nodeFD = table->T[fd];
		minFilsInd = (nodeFG->value > nodeFD->value) ? fd : fg; minFils = table->T[minFilsInd]; // On récupère le fils avec la plus petite valeur
		if (minFils->value < toSwap->value) { 
			// Les fils ont une plus grande valeur que leur père
			DTableSwapBoth(table, dictionnary, emplacement, minFilsInd); // On intervertit la position de la node et de son fils (avec mise à jour du dictionnaire)
			// Mise à jour des indices pour la prochaine itération
			emplacement = minFilsInd;
			fg = indFilsG(emplacement); fd = fg + 1;
			nodeFG = table->T[fg]; nodeFD = table->T[fd];
		} else {
			// La node est bien placée, on sort de la boucle
			break; 
		}
	}
	if (fg < table->used) { // Cas particulier on est arrivé "au bout" de l'arbre, il n'y a qu'un fils non nul
		nodeFG = table->T[fg];
		if (nodeFG->value < toSwap->value) {
			DTableSwapBoth(table, dictionnary, emplacement, fg);
			// Pas besoin d'actualiser car nous sommes au bout du tableau
		}
	}
	// Libération de la mémoire qui n'est plus utilisée
	free(dictionnary->T[*((int *)extracted->ptr)]); 
	dictionnary->T[*((int *)extracted->ptr)] = NULL; // Pas obligatoire, mais préférable
	free(extracted->ptr);
	return extracted;
}

void DTHeapIncreasePriority(Heap* H, void *position, int value) {
    DTable * table = H->heap;
    DTable * dictionnary = H->dict;
	int * pos = position;
    int emplacement = *((int *)dictionnary->T[*pos]); // Emplacement est l'indice de node dans le tableau table
	HNode * node = table->T[emplacement];
	if (node->value >= value) {
		int pere = indPere(emplacement); // Père est l'indice du père de node dans le tableau table
    	HNode *nodePere;
		while (pere >= 0) { // On remonte la node
			nodePere = table->T[pere];
			if (nodePere->value > value) { // Le père a une plus grande valeur que son fils
				DTableSwapBoth(table, dictionnary, emplacement, pere);
				// Mise à jour des indices pour la prochaine itération
				emplacement = pere;
				pere = indPere(emplacement);
				nodePere = table->T[pere];
			} else { // La node est bien placée, on sort de la boucle
				break;
			}
		}
	} else {
		int fg = indFilsG(emplacement), fd = fg + 1, minFilsInd; // fg, fd sont les indices des nodes dans le tableau table
    	HNode * nodeFG, * nodeFD, * minFils;
		while (fd < table->used) { // Cas général : les deux fils non NULL
			nodeFG = table->T[fg]; nodeFD = table->T[fd];
			minFilsInd = (nodeFG->value > nodeFD->value) ? fd : fg; minFils = table->T[minFilsInd];
			if (minFils->value < value) { // Les fils ont une plus petite valeur que leur père
				DTableSwapBoth(table, dictionnary, emplacement, minFilsInd);
				// Mise à jour des indices pour la prochaine itération
				emplacement = minFilsInd;
				fg = indFilsG(emplacement); fd = fg + 1;
				nodeFG = table->T[fg]; nodeFD = table->T[fd];
			} else {
				break;
			}
		}
		if (fg < table->used) { // Cas particulier : Un seul fils non nul, car on est "au bout" de l'arbre
			nodeFG = table->T[fg];
			if (nodeFG->value < value) {
				DTableSwapBoth(table, dictionnary, emplacement, fg);
				// Pas besoin d'actualiser pour une prochaine itération car nous sommes au bout du tableau
			}
		}
	}
	node->value = value; // Actualisation de la valeur de priorité de la node
}

void viewDTHeap(const Heap* H, void (*ptrf)(const void*)) {
	DTable * tab = H->heap;
	viewDTable(tab, ptrf);
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

void* CBTHeapInsert(Heap *H, int value, void *data) {
	HNode * node = newHNode(value, data);
	CBTree * tree = H->heap;
	CBTreeInsert(tree, node);
	TNode * toReturn = tree->last, * inserted = tree->last; // La nouvelle node est maintenant tree->last, il faut peut être la remonter
	HNode * comparedTo; // La node avec qui comparer
	while (inserted->parent != NULL) { // Tant qu'on est pas remonté jusque la racine
		comparedTo = inserted->parent->data;
		if (comparedTo->value <= value) { // On compare la priorité de la nouvelle node avec celle de son père
			break; // La node est bien placée, on sort de la boucle
		}
		// La node est mal placée
		CBTreeSwap(tree, inserted->parent, inserted); // On intervertit la position de la node et de son père
	}
	return toReturn;
}

HNode * CBTHeapExtractMin(Heap *H) {
	CBTree * tree = H->heap;
	CBTreeSwapRootLast(tree);
	HNode * extracted = CBTreeRemove(tree);
	if (tree->numelm == 0) return extracted; // L'arbre ne contenait qu'un élément, il n'y a plus rien à faire
	TNode * toSwap = tree->root, * minFils;
	HNode * left, * right, * min, * node = toSwap->data;
	while (toSwap->right != NULL) { // Cas général : les deux fils non NULL
		left = toSwap->left->data;
		right = toSwap->right->data;
		minFils = (left->value > right->value) ? toSwap->right : toSwap->left; // On récupère le fils qui à la plus grande priorité
		min = minFils->data;
		if (min->value < node->value) { // La priorité de la node est inférieure à celle de ses fils
			CBTreeSwap(tree, toSwap, minFils); // On l'échange avec le fils de plus grande priorité
		} else { // La node est bien placée, on sort de la boucle
			break;
		}
	}
	if (toSwap->left != NULL) { // Cas particulier : On est arrivé au bout de l'arbre, le noeud n'a qu'un fils
		left = toSwap->left->data;
		if (left->value < node->value) {
			CBTreeSwap(tree, toSwap, toSwap->left); // On échange le noeud avec son fils si besoin
		}
	}
	return extracted;
}

void CBTHeapIncreasePriority(Heap *H, void *tnode, int value) {
    TNode * cbtnode = tnode;
    TNode * minfils;
    HNode * node = cbtnode->data;
    CBTree * tree = H->heap;
    HNode * left, * right, * parent, * min;
	if (node->value >= value) { // On diminue la valeur de la priorité : Il faudra remonter la node
		while (cbtnode->parent != NULL) { // Tant qu'on est pas remonté jusqu'a la racine
			parent = cbtnode->parent->data;
			if (parent->value > value) { // La priorité de la node est inférieure à celle de son père
				CBTreeSwap(tree, cbtnode->parent, cbtnode); // On les échange
			} else { // La node est bien placée
				break;
			}
		}
	} else { // On augmante la valeur de la priorité : il faudra descendre la node
		while (cbtnode->right != NULL) { // Cas général : les deux fils non NULL
			left = cbtnode->left->data;
			right = cbtnode->right->data;
			minfils = (left->value > right->value) ? cbtnode->right : cbtnode->left; // On récupère le fils de plus petite valeur de priorité
			min = minfils->data;
			if (min->value < value) { // La priorité de la node est supérieure à celle de ses fils
				CBTreeSwap(tree, cbtnode, minfils); // On l'échange avec son fils de plus petite priorité
			} else { // La node est bien placée
				break; // On sort de la boucle
			}
		}
		if (cbtnode->left) { // Cas particulier : On est arrivé au bout de l'arbre, le noeud n'a qu'un fils
			left = cbtnode->left->data;
			if (left->value < value) {// La priorité de la node est supérieure à celle de son fils
				CBTreeSwap(tree, cbtnode, cbtnode->left);// On l'échange avec son fils
			}
		}
	}
	node->value = value; // Actualisation de la valeur de priorité
}

void viewCBTHeap(const Heap *H, void (*ptrf)()) {
	CBTree * tree = H->heap;
	printf("Affichage de l'arbre de façon préfixée\n");
	viewCBTree(tree, ptrf, 0);
	printf("Affichage de l'arbre de façon postfixée\n");
	viewCBTree(tree, ptrf, 1);
	printf("Affichage de l'arbre de façon infixée\n");
	viewCBTree(tree, ptrf, 2);
}

/**********************************************************************************
 * ORDERED-LIST HEAP
 **********************************************************************************/

void* OLHeapInsert(Heap *H, int value, void* data) {
	HNode * node = newHNode(value, data);
	List * OL = H->heap;
	LNode * iterator = OL->head; // On commence à parcourir la liste  par la tête
	HNode * comparedTo;
	while (iterator != NULL) { // Tant qu'on est pas arrivé en fin de liste
		comparedTo = iterator->data;
		if (comparedTo->value > value) { // Si la priorité du prochain noeud est supérieure à celle du noeud à insérer
			break; // On a trouver ou placer la node on sort de la boucle
		}
		iterator = iterator->suc;
	}
	if (iterator == OL->head) { // On ne s'est pas déplacé, la place de la node est en tête de liste
		listInsertFirst(OL, node); // L'insertion se fait en tête
		return OL->head;
	} else if (iterator == NULL) { // On et arrivé à la fin de la liste
		listInsertLast(OL, node); // L'insertion se fait en bout de liste
		return OL->tail;
	} else { //Cas general 
		listInsertAfter(OL, node, iterator->pred); // Insertion à l'endroit determiné précedement
		return iterator->pred; 
	}
}

HNode * OLHeapExtractMin(Heap *H) {
	List * OL = H->heap;
	LNode * node = listRemoveFirst(OL); // L'élément de priorité minimale est en tête de liste, car celle-ci est ordonnée
	HNode * extracted = node->data;
	free(node); // On libère la mémoire qui n'est plus utilisée
	return extracted;
}

void OLHeapIncreasePriority(Heap *H, void* lnode, int value) {
	List * OL = H->heap;
	LNode * modified = lnode, * left = modified->pred, * right = modified->suc;
	HNode * node = modified->data, * leftNode, * rightNode;
	if (value > node->value) { // On augmente la valeur de la priorité
		while (right != NULL) { // Le décalage va se faire vers la droite
			rightNode = right->data;
			if (value <= rightNode->value) {
				break;
			}
			listSwap(OL, modified, right);
			right = modified->suc;
		}
	} else { // On augmente la valeur de la priorité
		while (left != NULL) { // Le décalage va se faire vers la gauche
			leftNode = left->data;
			if (value < leftNode->value) {
				listSwap(OL,left, modified);
				left = modified->pred;
			} else {
				break;
			}
		}
	}
	node->value = value; // On actualise la priorité
}

void viewOLHeap(const Heap *H, void (*ptrf)()) {
	List * OL = H->heap;
	viewList(OL, ptrf);
}
