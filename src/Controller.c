#include "Controller.h"
#include "main.c"

JNIEXPORT void JNICALL Java_Controller_Dijkstra
(JNIEnv * env, jobject obj, jstring in, jstring out, jstring source, jstring type) {
    const char *fileIn = (*env)->GetStringUTFChars(env, in, NULL);
    const char *fileOut = (*env)->GetStringUTFChars(env, out, NULL);
    const char *sourceIn = (*env)->GetStringUTFChars(env, source, NULL);
    const char *typeTas = (*env)->GetStringUTFChars(env, type, NULL);
    Dijkstra(fileIn, fileOut, sourceIn, atoi(typeTas));
}
