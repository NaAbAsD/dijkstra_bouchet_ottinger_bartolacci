Une release du code est taggée et disponible dans Repository/tags.
C'est une version fonctionnelle possèdant son code C et son interface Java, toutes deux liées grâce à la JNI.
Ce tag est précieux car il contient notamment quelques informations par rapport au code et à sa compilation.
C'est également une version de secours au cas ou l'actuelle release ne fonctionnerait pas.

Le rapport de projet n'est pas présent dans cette version taggée, mais le sera dans la dernière accessible avant le 6 mai 23h59.