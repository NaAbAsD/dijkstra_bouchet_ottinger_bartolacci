IDIR = include
ODIR = obj
SDIR = src
JDIR = java
DDIR = data

CC = gcc
CFLAGS = -Wall -fPIC -I $(IDIR)
EXE = -o $(JDIR)/Dijkstra.so

PROG = dijkstra

JINCLUDES = -I/usr/lib/jvm/oracle-java8-jdk-amd64 -I/usr/lib/jvm/oracle-java8-jdk-amd64/include -I/usr/lib/jvm/oracle-java8-jdk-amd64/include/linux

_DEPS = dyntable.h tree.h list.h heap.h town.h road.h graph.h test.h Controller.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ= dyntable.o tree.o list.o heap.o town.o road.o graph.o test.o Controller.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

.PHONY : all noui delete

all : $(ODIR)/Controller.o $(OBJ)
	$(CC) -shared $(EXE) $^

$(ODIR)/Controller.o : $(SDIR)/Controller.c $(DEPS)
	$(CC) $(CFLAGS) $(JINCLUDES) -c -o $@ $<

$(ODIR)/%.o : $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

noui : $(PROG)

$(PROG) : $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(ODIR)/%.o : $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

delete: 
	rm -f $(ODIR)/*.o
	rm -f $(DDIR)/out.txt
	rm -f $(JDIR)/*.class
	rm $(PROG)
	#rm -f $(JDIR)/Dijkstra.so
