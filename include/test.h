#ifndef _TEST_H_
#define _TEST_H_

void test_list();

void viewListReverse(const List * L, void (*ptrF)());

void testListSuppression();

void testListSwap();

void testCBTreeInsert();

void testCBTreeSuppression();

void testCBTreeSwap();

void testCBTreeSwapRootLast();

void testDynTableInsertRemoveSwap();

void viewHNodeTest(const HNode *node);

void testOLHeapInsertChangePrioExtract();

void testCBTHeapInsertChangePrioExtract();

void testDTHeapInsertChangePrioExtract();

#endif // _TEST_H_
